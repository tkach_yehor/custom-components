export const ADD_ALERT_TO_LIST = 'ADD_ALERT_TO_LIST';
export const DELETE_ALERT_FROM_LIST = 'DELETE_ALERT_FROM_LIST';

const INITIAL_STATE = {
  alertList: [
    /**
     * List of alerts
     *
     * Alert object: {
     *   type: number,
     *   title: string,
     *   text: string
     * }
     */
  ]
};

export const LIST_OF_ALERT_TYPES = ['Error', 'Warning', 'Accept', 'Info'];

export const ERROR_ALERT = 0;
export const WARNING_ALERT = 1;
export const ACCEPT_ALERT = 2;
export const INFO_ALERT = 3;

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_ALERT_TO_LIST: {
      return {
        ...state,
        alertList: state.alertList.concat(action.payload)
      };
    }

    case DELETE_ALERT_FROM_LIST: {
      return {
        ...state,
        alertList: deleteFirstAlertFromList(state.alertList)
      };
    }

    default: {
      return state;
    }
  }
};

export const addAlertToList = item => {
  return dispatch => {
    dispatch({
      type: ADD_ALERT_TO_LIST,
      payload: item
    });
    setTimeout(() => {
      dispatch({
        type: DELETE_ALERT_FROM_LIST
      });
    }, 10000);
  };
};

const deleteFirstAlertFromList = list => {
  let result = [];

  for (let i = 1; i < list.length; i++) {
    result.push(list[i]);
  }

  return result;
};

export const deleteAlertFromList = () => {
  return dispatch => {
    dispatch({
      type: DELETE_ALERT_FROM_LIST
    });
  };
};
