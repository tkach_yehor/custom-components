import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import alert from './alert';

export default combineReducers({
  routing: routerReducer,
  alert
});
