import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import rootReducer from './redux';
import DevTools from './containers/DevTools';
import { persistState } from 'redux-devtools';

export const history = createHistory();

const initialState = {};
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];
let tools = [];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.devToolsExtension;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }

  tools.push(DevTools.instrument());
  tools.push(persistState(getDebugSessionKey()));
}

function getDebugSessionKey() {
  const matches = window.location.href.match(/[?&]debug_session=([^&#]+)\b/);
  return matches && matches.length > 0 ? matches[1] : null;
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers,
  ...tools
);

const store = createStore(rootReducer, initialState, composedEnhancers);

export default store;
