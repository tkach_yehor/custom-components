import React from 'react';

import './Alert.css';

const Alert = props => (
  <div className={'Alert ' + props.class} style={props.style}>
    <h6>{props.title}</h6>
    <div>{props.text}</div>
  </div>
);

export default Alert;
