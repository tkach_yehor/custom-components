import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { connect } from 'react-redux';

import Alert from './Alert';

import './AlertList.css';

import { LIST_OF_ALERT_TYPES } from '../../redux/alert';

class AlertList extends Component {
  componentWillMount = () => {
    this.root = document.createElement('div');
    document.body.appendChild(this.root);
  };

  componentWillUnmount = () => {
    document.body.removeChild(this.root);
  };

  render() {
    return ReactDOM.createPortal(
      <div className="AlertList">
        {this.props.alertList.map((item, index) => {
          return (
            <Alert
              key={index}
              class={LIST_OF_ALERT_TYPES[item.type]}
              title={item.title}
              text={item.text}
            />
          );
        })}
      </div>,
      this.root
    );
  }
}

const mapStateToProps = state => ({
  alertList: state.alert.alertList
});

export default connect(mapStateToProps)(AlertList);
