import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import './App.css';

import AlertList from '../AlertList';

import {
  ACCEPT_ALERT,
  ERROR_ALERT,
  WARNING_ALERT,
  INFO_ALERT,
  LIST_OF_ALERT_TYPES,
  addAlertToList
} from '../../redux/alert';

class App extends Component {
  handleAddAlertToList = type => {
    this.props.addAlertToList({
      type: type,
      title: LIST_OF_ALERT_TYPES[type],
      text: 'Some ' + LIST_OF_ALERT_TYPES[type] + ' alert'
    });
  };

  render() {
    return (
      <div>
        <button onClick={this.handleAddAlertToList.bind(this, ACCEPT_ALERT)}>
          {LIST_OF_ALERT_TYPES[ACCEPT_ALERT]}
        </button>
        <button onClick={this.handleAddAlertToList.bind(this, WARNING_ALERT)}>
          {LIST_OF_ALERT_TYPES[WARNING_ALERT]}
        </button>
        <button onClick={this.handleAddAlertToList.bind(this, ERROR_ALERT)}>
          {LIST_OF_ALERT_TYPES[ERROR_ALERT]}
        </button>
        <button onClick={this.handleAddAlertToList.bind(this, INFO_ALERT)}>
          {LIST_OF_ALERT_TYPES[INFO_ALERT]}
        </button>
        <br />
        <AlertList />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addAlertToList
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
